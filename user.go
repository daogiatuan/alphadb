package main

import (
	"encoding/gob"
	"fmt"
	"net/url"
	"os"
	"reflect"

	a "gitlab.com/daogiatuan/Alphadb/actions"
	"gitlab.com/daogiatuan/Alphadb/alpha"
)

func userinfo(id string, pass string) url.Values {
	i := url.Values{}
	i.Set("EmailAddress", id)
	i.Set("Password", pass)
	return i
}

func saveCre(info a.UserInfo) error {
	file, err := os.Create("cre.gob")
	if err == nil {
		encoder := gob.NewEncoder(file)
		encoder.Encode(info)
	}
	file.Close()
	return err
}

func loadCre(val *a.UserInfo) error {
	file, err := os.Open("cre.gob")
	if err == nil {
		decoder := gob.NewDecoder(file)
		err = decoder.Decode(val)
	}
	file.Close()
	return err
}

func processLogin(username string, password string, u *a.UserInfo) {
	if _, err := os.Stat("cre.gob"); os.IsNotExist(err) {
		fmt.Println("credential does not exist, call for login")
		*u = a.Login(userinfo(username, password))
		saveCre(*u)
	} else {
		loadCre(u)
		fmt.Println(u)
	}
	if username == u.Userinfo.Get("EmailAddress") || password == u.Userinfo.Get("Password") {
		fmt.Println("\n\n\nWe already have the old credential check if still valid")
		test := a.GetAlpha(*u)
		emp := alpha.Alphadata{}
		if reflect.DeepEqual(test, emp) {
			fmt.Println("Shoot cre is out of date and not valid login again")
			*u = a.Login(userinfo(username, password))
			saveCre(*u)
			fmt.Println(*u)
		}
	} else {
		fmt.Println("Shoot newuser to town, get new credential for the guy")
		*u = a.Login(userinfo(username, password))
		saveCre(*u)
	}
}
