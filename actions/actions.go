package actions

import (
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"net/url"
	"regexp"
	"strings"
	"time"

	"gitlab.com/daogiatuan/Alphadb/alpha"
)

// UserInfo Contain user information
type UserInfo struct {
	Cookie   []*http.Cookie
	Userinfo url.Values
}

var body io.Reader

// Login will get the credential,spit query info , need url.values type from user
func Login(info url.Values) UserInfo {
	fmt.Println("Getting the credential for alphadata......")
	body = strings.NewReader(info.Encode() + `&next=&g-recaptcha-response=&_xsrf=2%7Cbe1047cf%7Ca6288f461a8ab50e59afe3e592ac21b2%7C1544192940`)
	req, _ := http.NewRequest("POST", "https://www.worldquantvrc.com/login/process", body)

	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	req.Header.Set("Accept", "application/json")

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		fmt.Println("Error logging in")
	}
	test, _ := ioutil.ReadAll(resp.Body)
	fmt.Println(string(test))
	defer resp.Body.Close()

	return UserInfo{Cookie: resp.Cookies(), Userinfo: info}
}

// GetAlpha will get the response and store it into AlphaData struct
func GetAlpha(info UserInfo) alpha.Alphadata {
	fmt.Println("Requesting alphadata from server.......")
	body = strings.NewReader(`type=is&_xsrf=2%7Cbe1047cf%7Ca6288f461a8ab50e59afe3e592ac21b2%7C1544192940&fields=%5B%22Returns%22%2C%22Color%22%2C%22Universe%22%2C%22CodeType%22%2C%22Fitness%22%2C%22AlphaName%22%2C%22Hidden%22%2C%22Code%22%2C%22Margin%22%2C%22TurnOver%22%2C%22IsInOS%22%2C%22Region%22%2C%22IsTeamAlpha%22%2C%22DateCreated%22%2C%22Sharpe%22%2C%22Favorite%22%5D&clauses=%5B%7B%22Hidden%22%3A%5B%22%7B%2C%7D%22%2C0%5D%7D%2C%7B%22CodeType%22%3A%5B%22%7B%2C%7D%22%2C%22FLOWSEXPR%22%5D%7D%2C%7B%22IsInOS%22%3A%5B%22%7B%2C%7D%22%2C%220%22%5D%7D%5D&limit=%7B%22limit%22%3A%22100%22%2C%22pageNumber%22%3A1%7D&sort=%7B%22colName%22%3A%22Margin%22%2C%22sortOrder%22%3A%22DESC%22%7D`)
	req, _ := http.NewRequest("POST", "https://www.worldquantvrc.com/myalphas/alphadata", body)
	for _, cookie := range info.Cookie {
		req.AddCookie(cookie)
	}
	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	req.Header.Set("Accept", "application/json")

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		fmt.Println("Error getting alpha")
	}
	defer resp.Body.Close()

	// Retry in case the server response is low
	if resp.StatusCode == 504 || resp.StatusCode == 400 {
		fmt.Println("Not successful get the data as requested, get new credential, logging in , retry in 60s")
		time.Sleep(1 * time.Minute)
		GetAlpha(Login(info.Userinfo))
	} else {
		fmt.Println("Getting the data successfully, data returned")
	}
	data, _ := ioutil.ReadAll(resp.Body)
	fmt.Println(string(data))
	var alphas alpha.Alphadata
	json.Unmarshal(data, &alphas)
	return alphas
}

// GetPayout information
func GetPayout(info UserInfo) alpha.PayoutInfo {
	body := strings.NewReader(`_xsrf=2%7Cdbe9d14e%7C382b0e5587ad45a690f58aa066f83608%7C1546840433`)
	req, err := http.NewRequest("POST", "https://www.worldquantvrc.com/consultant/payoutsummary", body)
	if err != nil {
		// handle err
	}
	for _, cookie := range info.Cookie {
		req.AddCookie(cookie)
	}
	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	req.Header.Set("Accept", "application/json")

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		// handle err
	}
	data, _ := ioutil.ReadAll(resp.Body)
	defer resp.Body.Close()
	test, _ := regexp.Compile(`\[(.*?)\]`)
	var payout alpha.PayoutInfo
	json.Unmarshal(test.Find(data), &payout)
	return payout
}

// Get bonus information
func GetBonus(info UserInfo) alpha.BonusInfo {
	body := strings.NewReader(`_xsrf=2%7Cdbe9d14e%7C382b0e5587ad45a690f58aa066f83608%7C1546840433`)
	req, err := http.NewRequest("POST", "https://www.worldquantvrc.com/consultant/userdata", body)
	if err != nil {
		// handle err
	}
	for _, cookie := range info.Cookie {
		req.AddCookie(cookie)
	}
	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	req.Header.Set("Accept", "application/json")

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		// handle err
	}
	data, _ := ioutil.ReadAll(resp.Body)
	defer resp.Body.Close()
	var bonus alpha.BonusInfo
	//fmt.Println(string(data))
	json.Unmarshal(data, &bonus)
	return bonus
}
