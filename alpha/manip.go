package alpha

import (
	"strings"

	"github.com/tikikun/randname"
)

// Makealpha return isolated alpha string to manip
func (a alpha) Makealpha() (string, string) {
	cache := strings.Split(a.Code, ";")
	name := randname.NewLen(8)
	cache[len(cache)-1] = "\n" + name + " = " + cache[len(cache)-1]
	return norepicate(strings.Join(cache, ";") + ";"), name
}

// No replication
func norepicate(a string) string {
	alphas := strings.Split(a, ";")
	stringlist := randname.NewStringList(len(alphas), 8)
	for index, alpha := range alphas {
		cache := strings.Split(alpha, "=")
		cache[0] = stringlist[index]
		alpha = strings.Join(cache, "=")
	}
	return strings.Join(alphas, ";")
}

// Combine two alpha into one alpha
func Combine(a1 alpha, a2 alpha) (string, string) {
	alpha1, name1 := a1.Makealpha()
	alpha2, name2 := a2.Makealpha()
	name := randname.NewLen(8)
	return alpha1 + "\n\n" + alpha2 + "\n" + name + "=" + scale(name1) + "+" + scale(name2), name
}

func scale(a string) string {
	return "scale(" + a + ")"
}
