package alpha

import (
	"encoding/gob"
	"fmt"
	"os"
)

// Alpha and all of it handling method

type alpha struct {
	AlphaClientID string      `json:"AlphaClientId"`
	AlphaName     string      `json:"AlphaName"`
	Code          string      `json:"Code"`
	CodeType      string      `json:"CodeType"`
	Color         interface{} `json:"Color"`
	DateCreated   string      `json:"DateCreated"`
	Favorite      int         `json:"Favorite"`
	Fitness       float64     `json:"Fitness"`
	Hidden        int         `json:"Hidden"`
	IsInOS        int         `json:"IsInOS"`
	IsTeamAlpha   int         `json:"IsTeamAlpha"`
	Margin        float64     `json:"Margin"`
	Region        string      `json:"Region"`
	Returns       float64     `json:"Returns"`
	Sharpe        float64     `json:"Sharpe"`
	TurnOver      float64     `json:"TurnOver"`
	Universe      string      `json:"Universe"`
}

func (a *alpha) printName() {
	fmt.Println(a.AlphaName)
}

// Alphadata is the struct to parse the http response and get the datas
type Alphadata struct {
	Data    []alpha  `json:"data"`
	Header  []string `json:"header"`
	NumRows int      `json:"numRows"`
}

func (ad Alphadata) SaveToGob(filename string) error {
	file, err := os.Create(filename + ".gob")
	if err == nil {
		encoder := gob.NewEncoder(file)
		encoder.Encode(ad)
	}
	file.Close()
	return err
}

func (ad *Alphadata) SpawnFromGob() error {
	file, err := os.Open("alphadata.gob")
	if err == nil {
		decoder := gob.NewDecoder(file)
		err = decoder.Decode(ad)
	}
	file.Close()
	return err
}

// PayoutInfo
type PayoutInfo []struct {
	LocalPayout  float32 `json:"LocalPayout"`
	PayoutAmount float32 `json:"PayoutAmount"`
	PayoutPeriod string  `json:"PayoutPeriod"`
}

type BonusInfo struct {
	Result struct {
		PnlHistory []interface{} `json:"pnlHistory"`
		UserData   struct {
			AccBonus              interface{} `json:"AccBonus"`
			ActualWeightRank      int         `json:"ActualWeightRank"`
			City                  string      `json:"City"`
			ConsultantTier        string      `json:"ConsultantTier"`
			Country               string      `json:"Country"`
			EstBonusPayout        interface{} `json:"EstBonusPayout"`
			EstDiscBonus          interface{} `json:"EstDiscBonus"`
			ScoreUpdateDate       string      `json:"ScoreUpdateDate"`
			Sim3AlphaCount        interface{} `json:"Sim3AlphaCount"`
			Sim3Weight            interface{} `json:"Sim3Weight"`
			SimWeightRank         int         `json:"SimWeightRank"`
			TotalActualAlphaCount int         `json:"TotalActualAlphaCount"`
			TotalActualWeight     float64     `json:"TotalActualWeight"`
			TotalBonus            interface{} `json:"TotalBonus"`
			TotalSimAlphaCount    int         `json:"TotalSimAlphaCount"`
			TotalSimWeight        float64     `json:"TotalSimWeight"`
			UnivCity              string      `json:"UnivCity"`
			UnivCountry           string      `json:"UnivCountry"`
			University            string      `json:"University"`
			UserAlias             string      `json:"UserAlias"`
			UserMcorr             float64     `json:"UserMcorr"`
			UserOsIsRatio         float64     `json:"UserOsIsRatio"`
			UserTurnover          float64     `json:"UserTurnover"`
		} `json:"userData"`
		VrcPnl float64 `json:"vrcPnl"`
	} `json:"result"`
	Status bool `json:"status"`
}
