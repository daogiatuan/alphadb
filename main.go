package main

import (
	"flag"
	"fmt"

	a "gitlab.com/daogiatuan/Alphadb/actions"
)

var u a.UserInfo

func main() {
	var username = flag.String("u", "example@gmail.com", "input your username here")
	var password = flag.String("p", "12345678", "input your password here")
	var command = flag.String("cmd", "your command here", "input your command here")
	var alpha = flag.String("a", "your alpha here", "input your alpha here")
	flag.Parse()
	// fmt.Println(*username, *password)
	switch *command {
	case "getCre":
		processLogin(*username, *password, &u)
	case "getData":
		loadCre(&u)
		test := a.GetAlpha(u)
		fmt.Println(test)
	case "getPayout":
		loadCre(&u)
		payout := a.GetPayout(u)
		fmt.Println(payout[0].PayoutAmount, " :Today")
		fmt.Println(payout[1].PayoutAmount, " :Til now")
		fmt.Println(payout[2].PayoutAmount, " :Last month")
	case "getBonus":
		loadCre(&u)
		bonus := a.GetBonus(u)
		fmt.Println(bonus.Result.UserData.ActualWeightRank, " : Actual Rank")
		fmt.Println(bonus.Result.UserData.TotalBonus, ": Bonus")
	case "simulate":
		loadCre(&u)
		a.Simualte(*alpha, u)
	default:
		fmt.Println("Seem like you did not input a good command, try again")
	}
}
